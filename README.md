# Appdeploy

The appdeploy cookbook will deploy multiple jar to a server and start them with whatever options you have specified in the attribute blocks in the role.

# Requirements

#### Operating Systems
- `CentOS 6.X`

#### packages
- `java` - needs java to start the jar's

# Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:user][:name]</tt></td>
    <td>String</td>
    <td>User application should run as</td>
    <td><tt>nil</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:user][:group]</tt></td>
    <td>String</td>
    <td>User group application should run as</td>
    <td><tt>nil</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:user][:shell_enabled]</tt></td>
    <td>Boolean</td>
    <td>Should user be allowed to have login shell</td>
    <td><tt>false</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:home][:dir]</tt></td>
    <td>String</td>
    <td>Parent directory app and content should live</td>
    <td><tt>/srv/apps</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:home][:app_dir]</tt></td>
    <td>String</td>
    <td>Directory jar should live</td>
    <td><tt>/srv/apps/jars</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:home][:log_dir]</tt></td>
    <td>String</td>
    <td>Directory logs should live</td>
    <td><tt>/srv/apps/logs</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:home][:mode]</tt></td>
    <td>String</td>
    <td>Permissions on the application home directory</td>
    <td><tt>0755</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:download][:filename]</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:download][:authuser]</tt></td>
    <td>Boolean</td>
    <td>Authentication username for wget</td>
    <td><tt>nil</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:download][:authpass]</tt></td>
    <td>String</td>
    <td>Authentication password for wget</td>
    <td><tt>nil</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:download][:destination]</tt></td>
    <td>String</td>
    <td>Download destination for jars</td>
    <td><tt>/srv/apps/jars</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:logrotate][:enabled]</tt></td>
    <td>Boolean</td>
    <td>whether to enable logrotate for your applications</td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:rsyslog][:enabled]</tt></td>
    <td>Boolean</td>
    <td>whether to enable rsyslog with your applications</td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>[:appdeploy][:jvm_opts]</tt></td>
    <td>String</td>
    <td>Global jar options</td>
    <td><tt>-Xms1024M -Xmx1024M</tt></td>
  </tr>
</table>

License and Authors
-------------------

Author: Dallin Wright
