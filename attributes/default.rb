# User dependencies, will create this user the application will run as
default[:appdeploy][:user][:name] = 'tomcat'
default[:appdeploy][:user][:group] = 'tomcat'
default[:appdeploy][:user][:shell_enabled] = false

# Application directory dependencies, will create the structure for the application files to live. Required.
default[:appdeploy][:home][:dir] = '/srv/apps'
default[:appdeploy][:home][:app_dir] = '/srv/apps/bin'
default[:appdeploy][:home][:log_dir] = '/srv/apps/logs'
default[:appdeploy][:home][:mode] = '0755'

# Attributes required for download LWRP, can pass auth which remote_file cannot at time of authoring.
default[:appdeploy][:download][:url] = nil
default[:appdeploy][:download][:app_name] = nil
default[:appdeploy][:download][:filename] = nil
default[:appdeploy][:download][:authuser] = nil
default[:appdeploy][:download][:authpass] = nil
default[:appdeploy][:download][:destination] = '/srv/apps/bin'

# If you want logrotate setup
default[:appdeploy][:logrotate][:enabled] = true

# If you want rsyslog setup
default[:appdeploy][:rsyslog][:enabled] = true

# global jvm opts
# This can be a string, or anything you want that java understands
default[:appdeploy][:jvm_opts] = '-Xms1024M -Xmx1024M'
