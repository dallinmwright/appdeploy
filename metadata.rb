name 'appdeploy'
version '0.1.1'
maintainer 'Dallin Wright'
maintainer_email 'dallinwright01@gmail.com'
description 'App deploys'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))

depends 'java'
recommends 'logrotate'
recommends 'rsyslog'
