use_inline_resources

def whyrun_supported?
  false
end

action :create do
  log 'Downloading file initiated...'

  if !new_resource.authuser.nil? && !new_resource.authpass.nil?
    execute "wget --user #{new_resource.authuser} --password #{new_resource.authpass} -P #{new_resource.destination} #{new_resource.url}"
  else
    execute "wget -P #{new_resource.destination} #{new_resource.url}"
  end

  execute "chmod #{new_resource.mode} #{new_resource.destination}/#{new_resource.filename}"
  execute "chown #{new_resource.user}:#{new_resource.group} #{new_resource.destination}/#{new_resource.filename}"
  log 'Set file permissions on file'
end
