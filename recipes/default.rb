# This cookbook, gets aroudn a limitation in chef where you can only
# run a recipe once.

# Declare and install package dependencies
package 'wget'
package 'logrotate'

# Create user if missing, disabling shell login
user node[:appdeploy][:user][:name] do
  comment 'User managed by chef'
  shell node[:appdeploy][:user][:shell_enabled] ? '/bin/shell' : '/bin/false'
end

# Create application home dir
directory node[:appdeploy][:home][:dir] do
  owner node[:appdeploy][:home][:user]
  group node[:appdeploy][:home][:group]
  mode 0755
  recursive true
end

directory node[:appdeploy][:home][:log_dir] do
    owner node[:appdeploy][:home][:user]
    group node[:appdeploy][:home][:group]
    mode node[:appdeploy][:home][:mode]
    recursive true
end

directory node[:appdeploy][:home][:app_dir] do
    owner node[:appdeploy][:home][:user]
    group node[:appdeploy][:home][:group]
    mode node[:appdeploy][:home][:mode]
    recursive true
end

# The main part of the magic
node[:roles].each do |role|
  next unless role.start_with?('app-')
#  node.default[:appdeploy][:role] = role

# DELETE THIS BLOCK*********************************************
# This abstracts corp specific details from the world. 
if node[role][:base][:app_group].length > 0 
  app_group = node[role][:base][:app_group]
else
  app_group = 'edu/wgu'
end 
version = node[role][node.chef_environment][:version]
node.default[:appdeploy][:download][:url] = UrlHelper.url_set(role, version, app_group, node.chef_environment)
node.default[:appdeploy][:download][:filename] = "#{role.gsub('app-', '')}-#{version}.jar"
app_name = role.gsub('app-', '') 
node.default[:appdeploy][:app_name] = app_name
# DELETE THIS***************************************************

  # Section for App setup
  appdeploy_download node[role][node.chef_environment][:version] do
    filename node[:appdeploy][:download][:filename]
    url node[:appdeploy][:download][:url]

    unless node[:appdeploy][:download][:authuser].nil?
      authuser node[:appdeploy][:download][:authuser]
    end

    unless node[:appdeploy][:download][:authpass].nil?
      authpass node[:appdeploy][:download][:authpass]
    end

    destination node[:appdeploy][:home][:app_dir]
    user node[:appdeploy][:user][:name]
    group node[:appdeploy][:user][:group]
    mode node[:appdeploy][:home][:mode]
    not_if { ::File.exist?("#{node[:appdeploy][:home][:app_dir]}/#{node[:appdeploy][:download][:filename]}") }
  end

  # Generate init service template
  template node[:appdeploy][:download][:filename] do
    path "/etc/init.d/#{node[:appdeploy][:app_name]}"
    source 'app.init.erb'
    variables ({ node_obj: node[role]})
    mode '0755'
  end

  service node[:appdeploy][:app_name] do
    supports restart: true, start: true, stop: true
    action [:start, :enable]
    subscribes :restart, "template[#{node[:appdeploy][:download][:filename]}", :immediately
  end

  # Setup logrotate, if enabled
  if node[:appdeploy][:logrotate][:enabled].to_s == 'true'
    package 'logrotate'

    template "/etc/logrotate.d/#{node[:appdeploy][:app_name]}" do
      action :create
      source 'app.logrotate.erb'
      mode '0644'
    end
  end

  # Setup rsyslog, if enabled
  if node[:appdeploy][:rsyslog][:enabled].to_s == 'true'
    package 'rsyslog'

    service 'rsyslog' do
      action [:start, :enable]
    end

    template '/etc/rsyslog.d/.conf' do
      source 'app.rsyslog.conf.erb'
      mode '0644'
      notifies :reload, 'service[rsyslog]', :immediately
    end
  end
end
