actions :create

default_action :create

attribute :filename, kind_of: String, required: true
attribute :url, kind_of: String, required: true
attribute :destination, kind_of: String, required: true
attribute :user, kind_of: String, required: true
attribute :group, kind_of: String, required: true
attribute :mode, kind_of: String, required: true
attribute :authuser, kind_of: String
attribute :authpass, kind_of: String
